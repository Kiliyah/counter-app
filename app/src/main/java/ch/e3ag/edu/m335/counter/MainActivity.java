package ch.e3ag.edu.m335.counter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

// not finished yet / saving doesn't work (SharedPreferences --- crashed -> removed it for now)
public class MainActivity extends AppCompatActivity {

    private int counter;
    private TextView counterValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        counterValue = (TextView) findViewById(R.id.counter);

        counter = 0;
        saveCounter();
    }

    public void increaseCounter(View view) {
        counter++;
        saveCounter();
    }

    public void decreaseCounter(View view) {
        if(counter != 0) {
            counter--;
        }
        saveCounter();
    }

    public void resetCounter(View view) {
        counter = 0;
        saveCounter();
    }

    private void saveCounter() {
        counterValue.setText(counter + "");
    }
}